<?php
function esOpcionMenuActiva(string $opcionMenu) : bool
{
    if (strpos($_SERVER['REQUEST_URI'] , $opcionMenu) !== false)
        return true;
    
        return false;
}

function existeOpcionMenuActivaEnArray(array $opcionesMenu) : bool
{
    foreach ($opcionesMenu as $opcionesMenu)
    {
        if (esOpcionMenuActiva($opcionesMenu) === true)
        return true;
    }
    retun false;
}